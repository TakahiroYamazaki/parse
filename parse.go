package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"golang.org/x/text/encoding/japanese"
	"golang.org/x/text/transform"
)

func main() {
	filename := os.Args[1]
	search := os.Args[2]

	fmt.Printf("%s:%s", filename, search)
	// ファイルオープン
	fp, err := os.Open(filename)
	if err != nil {
		// エラー処理
	}
	defer fp.Close()

	scanner := bufio.NewScanner(transform.NewReader(fp, japanese.ShiftJIS.NewDecoder()))

	for scanner.Scan() {
		// ここで一行ずつ処理
		text := scanner.Text()
		if strings.Contains(text, search) {
			fmt.Println(text)
		}

	}

	if err = scanner.Err(); err != nil {
		// エラー処理
	}
}
